# Gasparini-etal-2024-GRL-CRH-submitted

Scripts and data used in the Gasparini et al., 2024 submitted manuscript to GRL

I. Scripts

1. The template script used for processing the RCEMIP data is in:
/scripts/postprocessing/regridding_averaging_merging_saving_RCEMIP.ipynb

2. Plotting scripts used in the main part of the paper and in the supplement are in:
/scripts/scripts_plotting/

II. Data

1. Observations & Satellite retrievals (used in Figures 4, S5):
Processed SST and CIRS-ML radiative heating data is available in:
/data/observations/

2. RCEMIP postprocessed data (used in Figures 4 and 5, Figures S4 and S6):
/data/RCEMIP_processed/

3. Radiative transfer model data (used in Figure 1, Figure S1, Supplemental Data Table 1):
/data/radiative-transfer-model/

4. Radiative transfer model data settings and namelist parameters
/data/radiative-transfer-model/model_settings/

5. SAM model data (used in Figures 2, 3, S2,S3)
/data/SAM/

6. SAM model data settings and namelist parameters
/data/SAM/model_settings/

III. Figures
The figures used in the manuscript are in folders
/figures/main/    and
/figures/supplement/

